<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Login Rotues
Route::get('/','LoginController@index');
Route::post('secure-login','LoginController@secure_login');

// Courses Rotues 
Route::prefix('courses')->group(function () {
    // get requests
    Route::get('/','CoursesController@index');
    Route::get('/edit/{courses_id}', 'CoursesController@edit');
    Route::get('/destroy/{courses_id}', 'CoursesController@destroy');

    Route::get('/subjects/{courses_id}', 'CoursesController@show');
    Route::get('/subjects/edit/{subjects_id}/{courses_id}', 'CoursesController@edit_subjects');
    Route::get('/subjects/destroy/{subjects_id}/{courses_id}', 'CoursesController@destory_subjects');
    


    // post requests
    Route::post('/store', 'CoursesController@store');
    Route::post('/update/{courses_id}', 'CoursesController@update');

    Route::post('/subjects/store', 'CoursesController@store_subjects');
    Route::post('/update/subjects/{subjects_id}', 'CoursesController@update_subjects');
});

// Student Rotues 
Route::prefix('students')->group(function () {
    // get requests
    Route::get('/','StudentsController@index');

    // post requests
    Route::post('/store','StudentsController@store');
});


// Dashboard Rotues 
Route::prefix('dashboard')->group(function () {
    // get requests
    Route::get('/','DashboardController@index');
});


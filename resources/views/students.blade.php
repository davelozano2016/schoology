@extends('layouts.app')
@section('container')
   <!-- Begin Page Content -->
   <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{$title}}</h1>
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
            <span style="display:block">{{$error}}</span>
            @endforeach
        </div>
    @endif

    @if($message = Session::get('message'))
        <div class="alert {{Session::get('alert')}}">
            {{$message}}
        </div>
    @endif

    <div class="row">
        <div class="col-md-4 col-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Student Details</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <form method="POST" action="{{url('students/store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">Image</label>
                            <input type="file" name="image" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Course</label>
                            <select name="courses_id" class="form-control">
                                @foreach($course as $data)
                                    <option value="{{Crypt::encryptString($data->id)}}">{{$data->course}}</option>
                                @endforeach
                            </select>
                        </div>

                        

                        <div class="form-group">
                            <label for="">Email Address</label>
                            <input type="email" name="email" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="text" readonly value="123456789" name="password" class="form-control">
                        </div>

                        <div class="float-right">
                            <button class="btn btn-primary">Add Student</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">List Of All Students</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width:1px">#</th>
                                    <th style="width:1px"></th>
                                    <th>Name</th>
                                    <th>Course</th>
                                    <th>Email</th>
                                    <th style="width:1px">Status</th>
                                    <th style="width:1px"></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th style="width:1px">#</th>
                                    <th style="width:1px"></th>
                                    <th>Name</th>
                                    <th>Course</th>
                                    <th>Email</th>
                                    <th style="width:1px">Status</th>
                                    <th style="width:1px"></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @php $i=1; @endphp
                                @foreach($students as $student)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td><img src="{{asset('assets/img')}}/{{$student->image}}" style="width:36px;height:36px" alt=""></td>
                                        <td>{{$student->name}}</td>
                                        <td>{{$courses[$student->id]->course}}</td>
                                        <td>{{$student->email}}</td>
                                        <td>
                                            @if($student->status == 0)
                                                <span class="badge badge-success">Active</span>
                                            @else
                                                <span class="badge badge-danger">Inactive</span>
                                            @endif
                                        </td>
                                        <th><a href="{{url('students/edit')}}/{{Crypt::encryptString($student->id)}}"><i class="fa fa-fw fa-eye"></i></a></th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

</div>

@section('custom')
<script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
<script>
    $('.students').addClass('active');
</script>
@endsection
<!-- /.container-fluid -->
@endsection
@extends('layouts.app')
@section('container')
   <!-- Begin Page Content -->
   <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{$title}}</h1>

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
            <span style="display:block">{{$error}}</span>
            @endforeach
        </div>
    @endif

    @if($message = Session::get('message'))
        <div class="alert {{Session::get('alert')}}">
            {{$message}}
        </div>
    @endif

    <div class="row">
        <div class="col-md-4 col-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Subject Details</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">

                    <form action="{{url('courses/update/subjects')}}/{{Crypt::encryptString($result->id)}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Subject</label>
                            <input type="text" name="subject" autofocus value="{{$result->subject}}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Year</label>
                            <select name="year" class="form-control" id="">
                                <option value="First"  {{$result->year == 'First'  ? 'selected' : ''}}>First Year</option>
                                <option value="Second" {{$result->year == 'Second' ? 'selected' : ''}}>Second Year</option>
                                <option value="Third"  {{$result->year == 'Third'  ? 'selected' : ''}}>Third Year</option>
                                <option value="Fourth" {{$result->year == 'Fourth' ? 'selected' : ''}}>Fourth Year</option>
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="">Semester</label>
                            <select name="semester" class="form-control" id="">
                                <option value="First" {{$result->semester == 'First' ? 'selected' : ''}}>First Semester</option>
                                <option value="Second" {{$result->semester == 'Second' ? 'selected' : ''}}>Second Semester</option>
                            </select>
                        </div>

                        <div class="float-right">
                            <button class="btn btn-primary">Save Changes</button>
                        </div>
                        
                        <div class="float-left">
                            <a href="{{url('courses/subjects/destroy')}}/{{Crypt::encryptString($result->id)}}/{{$courses_id}}" class="btn btn-danger">Delete Subject</a>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>

        <div class="col-md-8 col-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">List Of All Subjects</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width:1px">#</th>
                                    <th>Subject</th>
                                    <th style="width:1px">Year</th>
                                    <th style="width:1px">Semester</th>
                                    <th style="width:1px">Topic</th>
                                    <th style="width:1px"></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th style="width:1px">#</th>
                                    <th>Subject</th>
                                    <th style="width:1px">Year</th>
                                    <th style="width:1px">Semester</th>
                                    <th style="width:1px">Topic</th>
                                    <th style="width:1px"></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @php $i=1; @endphp
                                @foreach($subjects as $subject)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$subject->subject}}</td>
                                        <td class="text-center">{{$subject->year}}</td>
                                        <td class="text-center">{{$subject->semester}}</td>
                                        <td class="text-center"><a href="{{url('courses/subjects')}}/{{Crypt::encryptString($subject->id)}}/{{Crypt::encryptString($courses_id)}}">{{$count[$subject->id]}}</a></td>
                                        <th><a href="{{url('courses/subjects/edit')}}/{{Crypt::encryptString($subject->id)}}/{{$courses_id}}"><i class="fa fa-fw fa-eye"></i></a></th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    

</div>

    @section('custom')
    <script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
    <script>
        $('.course').addClass('active');
    </script>
    @endsection
<!-- /.container-fluid -->
@endsection
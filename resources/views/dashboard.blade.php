@extends('layouts.app')
@section('container')
   <!-- Begin Page Content -->
   <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{$title}}</h1>

    {{-- <div class="row">
        <div class="col-md-12 col-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    
                </div>
            </div>
        </div>
    </div> --}}

</div>
@section('custom')
<script>
    $('.dashboard').addClass('active');
</script>
@endsection
<!-- /.container-fluid -->
@endsection
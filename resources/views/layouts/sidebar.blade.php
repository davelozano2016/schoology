<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggle toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item dashboard">
        <a class="nav-link" href="{{url('dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <li class="nav-item course">
        <a class="nav-link" href="{{url('courses')}}">
            <i class="fas fa-fw fa-book"></i>
            <span>Courses</span></a>
    </li>

    <li class="nav-item students">
        <a class="nav-link" href="{{url('students')}}">
            <i class="fas fa-fw fa-user"></i>
            <span>Students</span></a>
    </li>

</ul>
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    //

    protected $table = 'subjects';
    protected $primaryKey = 'id';
    protected $fillable = ['courses_id','year','semester','subject'];

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\Course;
use App\Topic;
use App\Subjects;

use Session;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Courses';
        $courses = Course::get();
        $count = [];
        foreach($courses as $course) {
            $count[$course->id] = Subjects::where(['courses_id' => $course->id])->count();
        }

        return view('courses',compact('title','courses','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request,[
            'course' => 'required',
        ]);

        $query = Course::where(['course' => $request->course])->exists();
        if($query) {
            return back()->with(['message' => 'Course already exist.','alert' => 'alert-danger']);
        } else {
            Course::create(['course' => $request->course]);
            return back()->with(['message' => 'New course has been added.','alert' => 'alert-success']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $courses_id = Crypt::decryptString($id);
        $course     = Course::where(['id' => $courses_id])->first();
        $subjects   = Subjects::where(['courses_id' => $courses_id])->get();
        $title      = $course->course;
        $count      = [];
        foreach($subjects as $subject) {
            $count[$subject->id] = Topic::where(['subjects_id' => $subject->id])->count();
        }

        return view('subjects',compact('title','subjects','count','courses_id'));
    }

    public function edit_subjects($id,$courses_id)
    {
        //

        $subjects_id = Crypt::decryptString($id);
        $title      = 'Courses';
        $subjects   = Subjects::whereNotIn('id',[$subjects_id])->get();
        $result     = Subjects::where(['id' => $subjects_id])->first();
        $count      = [];
        foreach($subjects as $subject) {
            $count[$subject->id] = Topic::where(['subjects_id' => $subject->id])->count();
        }


        return view('edit-subjects',compact('title','subjects','result','count','courses_id'));
    }

    public function update_subjects(Request $request, $id)
    {
        $this->validate($request,[
            'subject'  => 'required',
            'year'     => 'required',
            'semester' => 'required'
        ]);


        $subjects_id = Crypt::decryptString($id);
        $query = Subjects::where(['id' => $subjects_id])->update(['subject' => $request->subject,'year' => $request->year,'semester' => $request->semester]);
        if($query) {
            return back()->with(['message' => 'Subject has been updated','alert' => 'alert-success']);
        } 
    }

    public function store_subjects(Request $request)
    {
        //

        $this->validate($request,[
            'subject'    => 'required',
            'year'       => 'required',
            'semester'   => 'required',
            'courses_id' => 'required'
        ]);

        $courses_id = Crypt::decryptString($request->courses_id);
        $query      = Subjects::where(['courses_id' => $courses_id,'subject' => $request->subject,'semester' => $request->semester])->exists();
        if($query) {
            return back()->with(['message' => 'Subject already exist.','alert' => 'alert-danger']);
        } else {
            Subjects::create(['courses_id' => $courses_id,'subject' => $request->subject,'year' => $request->year,'semester' => $request->semester]);
            return back()->with(['message' => 'New subject has been added.','alert' => 'alert-success']);
        }
    }

    public function destory_subjects($id,$courses_id)
    {
        //
        $subjects_id = Crypt::decryptString($id);
        $query = Subjects::where(['id' => $subjects_id])->delete();
        if($query) {
            return redirect('courses/subjects/'.$courses_id)->with(['message' => 'Subject has been deleted','alert' => 'alert-success']);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $courses_id = Crypt::decryptString($id);
        $title      = 'Courses';
        $courses    = Course::whereNotIn('id',[$courses_id])->get();
        $result     = Course::where(['id' => $courses_id])->first();
        $count      = [];
        foreach($courses as $course) {
            $count[$course->id] = Subjects::where(['courses_id' => $course->id])->count();
        }

        return view('edit-courses',compact('title','courses','result','count'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'course' => 'required',
        ]);
        $courses_id = Crypt::decryptString($id);
        $query = Course::where(['id' => $courses_id])->update(['course' => $request->course]);
        if($query) {
            return back()->with(['message' => 'Course has been updated','alert' => 'alert-success']);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $courses_id = Crypt::decryptString($id);
        $query = Course::where(['id' => $courses_id])->delete();
        if($query) {
            return redirect('courses')->with(['message' => 'Course has been deleted','alert' => 'alert-success']);
        }
    }
}

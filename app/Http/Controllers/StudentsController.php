<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Course;
use Session;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Students';
        $students = User::where(['user_type' => 0])->get();
        $courses = [];
        foreach($students as $student) {
            $courses[$student->id] = Course::where(['id' => $student->courses_id])->first();
        }
        $course = Course::all();
        return view('students',compact('title','students','courses','course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name'       => 'required',
            'courses_id' => 'required',
            'email'      => 'required|email',
            'password'   => 'required'
        ]);

        $query = User::where(['email' => $request->email])->exists();
        if($query) {
            return back()->with(['message' => 'Email already exist.','alert' => 'alert-danger']);
        } else {
            User::create([
                'courses_id'=> Crypt::decryptString($request->courses_id),
                'name'      => $request->name,
                'image'     => 'default.png',
                'email'     => $request->email,
                'password'  => Hash::make($request->password),
                'user_type' => 0,
                'status'    => 0,
            ]);
            return back()->with(['message' => 'New student has been added.','alert' => 'alert-success']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

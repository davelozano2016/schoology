<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = array(
            'courses_id' => 1,
            'name'       => 'Juan Dela Cruz',
            'image'      => 'default.png',
            'email'      => 'juandelacruz@gmail.com',
            'password'   => Hash::make('123456789'),
            'user_type'  => 1,
            'status'     => 0,
        );
        User::create($data);
    }
}

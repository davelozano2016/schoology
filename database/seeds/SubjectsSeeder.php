<?php

use Illuminate\Database\Seeder;
use App\Subjects;

class SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $data = array(
            ['courses_id' => 1,'year' => 'First','semester' => 'First','subject' => 'Introduction to Information Technology'],
        );

        foreach($data as $key => $array) {
            Subjects::create($array);
        }
    }
}

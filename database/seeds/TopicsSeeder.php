<?php

use Illuminate\Database\Seeder;
use App\Topic;

class TopicsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = array(
            ['subjects_id' => 1,'topic' => 'Introduction to database'],
        );

        foreach($data as $key => $array) {
            Topic::create($array);
        }
    }
}

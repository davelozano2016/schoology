<?php

use Illuminate\Database\Seeder;

use App\Course;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $data = array(
            ['course' => 'Bachelor of Science in Information Technology'],
            ['course' => 'Bachelor of Science in Computer Science'],
        );

        foreach($data as $key => $array) {
            Course::create($array);
        }
    }
}
